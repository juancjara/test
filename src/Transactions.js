import React from "react";
import { fromWei } from "web3-utils";
import { Table, TableHeader, TableRow, TableCell, Text } from "@aragon/ui";

import useEtherTransactions from "./context/transactions-context";
import Button from "./Button";
import { shortName } from "./utils";

const Transactions = props => {
  const { hasMore, fetchMore, fetching, transactions } = useEtherTransactions({
    blockNumber: props.match.params.hash
  });

  return (
    <Table
      header={
        <TableRow>
          <TableHeader title="hash" />
          <TableHeader title="from" />
          <TableHeader title="to" />
          <TableHeader title="value" />
          <TableHeader title="Gas" />
          <TableHeader title="Gas Price" />
        </TableRow>
      }
    >
      {transactions.map(({ hash, from, to, value, gasPrice, gas }) => (
        <TableRow key={hash}>
          <TableCell>
            <Text>{shortName(hash)}</Text>
          </TableCell>
          <TableCell>
            <Text>{shortName(from)}</Text>
          </TableCell>
          <TableCell>
            <Text>{shortName(to)}</Text>
          </TableCell>
          <TableCell>
            <Text>{fromWei(value, "ether")}</Text>
          </TableCell>
          <TableCell>
            <Text>{gas}</Text>
          </TableCell>
          <TableCell>
            <Text>{gasPrice}</Text>
          </TableCell>
        </TableRow>
      ))}
      {hasMore && (
        <TableRow>
          <TableCell colSpan={6}>
            <Button mode="strong" disabled={fetching} onClick={fetchMore}>
              Fetch more
            </Button>
          </TableCell>
        </TableRow>
      )}
    </Table>
  );
};

export default Transactions;
