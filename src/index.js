import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Main, Root } from "@aragon/ui";
import * as serviceWorker from "./serviceWorker";

/*
Challenge
DESCRIPTION
Build a small app to explore the latest blocks on Ethereum. The goal of the app is to provide a way to glance at the recent Ether transfers happening on the blockchain.

REQUIREMENTS
Display the ten latest blocks.
Allow the user to see the transactions from a block. Only the transactions sending Ether should be displayed.
Allow the user to see some details about a transaction.

NOTES
You can use any library you feel comfortable with.
The app will be loaded in a browser with MetaMask.
Feel free to interpret the requirements in any way that you think could be interesting.
The app doesn’t need to follow the Aragon visual identity.
Don’t hesitate to ask any question to the team :-)
*/

/*
const web3 = new Web3(Web3.givenProvider || "ws://localhost:8546", null, {});
console.log(web3);
*/

ReactDOM.render(
  <Root.Provider>
    <Main>
      <App />
    </Main>
  </Root.Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
