import React, { useEffect, useState } from "react";
import fecha from "fecha";
import web3 from "./web3";
import Q from "q";
import { Link } from "react-router-dom";
import {
  Table,
  TableHeader,
  TableRow,
  TableCell,
  Text,
  CircleGraph,
  SafeLink
} from "@aragon/ui";

const NUM_BLOCKS = 10;

web3.setUp();

const useLatestBlocks = () => {
  const [fetching, setFetching] = useState(false);
  const [blocks, setBlocks] = useState([]);

  useEffect(() => {
    const fetchLatestBlocks = async () => {
      setFetching(true);
      const blockNumber = await web3.getBlockNumber();
      const blocks = await Q.all(
        [...Array(NUM_BLOCKS)].map((_, i) => web3.getBlock(blockNumber - i))
      );
      setFetching(false);
      setBlocks(blocks);
    };
    fetchLatestBlocks();
  }, []);

  return {
    fetching,
    blocks
  };
};

const Blocks = () => {
  const { fetching, blocks } = useLatestBlocks();
  return (
    <Table
      header={
        <TableRow>
          <TableHeader title="Block" />
          <TableHeader title="Miner" />
          <TableHeader title="# transactions" />
          <TableHeader title="Gas" />
          <TableHeader title="Date" />
        </TableRow>
      }
    >
      {blocks.map(
        ({ number, miner, transactions, gasLimit, gasUsed, timestamp }) => (
          <TableRow key={number}>
            <TableCell>
              <Link to={`/${number}`}>
                <Text>{number}</Text>
              </Link>
            </TableCell>
            <TableCell>
              <SafeLink
                target="_blank"
                href={`https://etherscan.io/address/${miner}`}
              >
                miner
              </SafeLink>
            </TableCell>
            <TableCell>
              <Text>{transactions.length}</Text>
            </TableCell>
            <TableCell>
              <CircleGraph size={60} value={gasUsed / gasLimit} />
            </TableCell>
            <TableCell>
              <Text>{fecha.format(timestamp * 1000, "MM-DD-YY HH:mm")}</Text>
            </TableCell>
          </TableRow>
        )
      )}
    </Table>
  );
};

export default Blocks;
